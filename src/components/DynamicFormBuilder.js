import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import PropTypes from "prop-types";

const DynamicFormBuilder = ({ formJSON, onSubmit }) => {
  const [data, setData] = useState({});

  const handleInputChange = (id, value) => {
    setData((savedInfo) => ({ ...savedInfo, [id]: value }));
  };

  const handleCheckboxChange = (id, checked) => {
    setData((savedInfo) => ({
      ...savedInfo,
      [id]: checked ? true : undefined,
    }));
  };

  const submitFunc = (e) => {
    e.preventDefault();
    onSubmit(data);
  };

  return (
    <div style={{ padding: "30px" }} className="container form-wrapper">
      <div style={{ padding: "20px 0" }} className="form-heading">
        <h1>Form Builder Telerik</h1>
      </div>
      <form onSubmit={submitFunc}>
        {formJSON.map((formSection, index) => (
          <div key={index} className="mb-3">
            {formSection.fields.map((field) => (
              <div key={field.id} className="mb-3">
                <label htmlFor={field.id} className="form-label">
                  {field.label}{" "}
                  {field.required && <span className="text-danger">*</span>}
                </label>
                {field.type === "text" && (
                  <input
                    type="text"
                    id={field.id}
                    className="form-control"
                    placeholder={field.placeholder}
                    value={data[field.id] || ""}
                    onChange={(e) =>
                      handleInputChange(field.id, e.target.value)
                    }
                    required={field.required}
                  />
                )}
                {field.type === "select" && (
                  <select
                    id={field.id}
                    className="form-select"
                    value={data[field.id] || ""}
                    onChange={(e) =>
                      handleInputChange(field.id, e.target.value)
                    }
                    required={field.required}
                  >
                    <option value="" disabled hidden>
                      Select an option
                    </option>
                    {field.options.map((option, optionIndex) => (
                      <option key={optionIndex} value={option.label}>
                        {option.label}
                      </option>
                    ))}
                  </select>
                )}
                {field.type === "checkbox" && (
                  <div className="form-check">
                    <input
                      type="checkbox"
                      id={field.id}
                      className="form-check-input"
                      checked={data[field.id] || false}
                      onChange={(e) =>
                        handleCheckboxChange(field.id, e.target.checked)
                      }
                    />
                    <label htmlFor={field.id} className="form-check-label">
                      {field.label}
                    </label>
                  </div>
                )}
              </div>
            ))}
          </div>
        ))}
        <div className="submit-button">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

DynamicFormBuilder.propTypes = {
  formJSON: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default DynamicFormBuilder;
