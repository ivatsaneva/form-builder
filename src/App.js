// App.js
import React from 'react';
import formJSON from './data/formData';
import DynamicFormBuilder from './components/DynamicFormBuilder';

function App() {
  const handleSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="container">
      <DynamicFormBuilder formJSON={formJSON} onSubmit={handleSubmit} />
    </div>
  );
}

export default App;
